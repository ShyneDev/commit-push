# How to start

## Tools

You will need a toolchain, so I recommend you these tools before start (if you are on a windows OS).

- A Text Editor: [Visual Studio Code](https://code.visualstudio.com/)
	- Visual Studio Code, not Visual Studio. VS Code is a text editor, VS is an IDE.
	- You can use vim, emacs, sublime, notepad++, or whatever if you prefer
- A version control software: [git](https://git-scm.com/)
- [Bash on Linux](https://www.howtogeek.com/249966/how-to-install-and-use-the-linux-bash-shell-on-windows-10/) (this works only if you have windows 10)
	- if you use windows 7 or 8 I will tell you something else

## Proceed

You will have to clone the project, create a file, commit modification and push it. To be able to do that you will need first to configure git and be allowed to clone the project.

### Create a key

You will have to create a private and public key to access to the first repo. For that we will use the ssh-keygen program.

```
$ ssh-keygen
```

By default it will store the key in `~/.ssh/id_rsa`. Press enter, and you can bundle a password to it if you want (you don't need to). This file is basicaly like the one you have for your bank. You will get two files: a private key `id_rsa` and a public key `id_rsa.pub`. You can share the public one with anyone, but never the private.

Upload the public key to here: https://gitlab.hanyang.co.kr/profile/keys

```
$ cat ~/.ssh/id_rsa.pub
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCiIh//ZWaM1Eo6GBjlEQLjVgfLS1VQb9+E+MdlvDiSbVBKh3Dxh7zkbSZjhks55aaSZUHxvyIn8TrW/daQkgUttod/5YsWJC9tat2NB+b/qmgP4275gQ5VBWBE+JikdOlToIuKBZnU/pOj20zxPcjGRuDn23X95uWk13FtNYqQh+KU58oF/D9zShtzU5Hi+/QZ7jqbfQR7KT5U93dv3jzs5LDB/CO52Th+Obr3b8XHo9wKw8psJc/HEr/6a7OtEMpkywy275L92r8s+jzcvbriPvRwxmVLhYGXJNmtNwJetxcMybDtaW3RfT9n2gdqB8VnLMvszZyEQ1iaKhj6jXDf wolf@DESKTOP-VV9JATF
```

Copy and paste this to the box this way:

![image](/uploads/1b11a1fb95dd821302e7c2bd88e9f188/image.png)

![image](/uploads/e4da459e36d5e964a16a42f1eccd1f8a/image.png)

Also get the keys both on windows and linux side. Copy your private key in the folder .ssh in linux and windows or create different keys and upload the second one the same way.

### Fork

Fork this project: https://gitlab.subak.ovh/learn/commit-push (clicking on the fork button). You will be able to get the project on your own page.

### Clone

Go in your work folder, for example ~/work ou ~/projects. There clone the repository this way:

```
git clone git@gitlab.subak.ovh:your-user-name/commit-push.git
```

Then you will have a new folder called commit-push. Go inside.

### Commit and Push

Last step, create a directory called `ex00`, put inside a file called `hello` with inside this string: `world`.

It should look like this:

```
wolf@BALTHAZAR:~/work/commit-push$ ls -l
total 4
drwxrwxrwx 1 wolf wolf  512 May 23 11:33 ex00
-rw-rw-rw- 1 wolf wolf 2183 May 23 11:32 README.md
wolf@BALTHAZAR:~/work/commit-push$ ls -l ex00/
total 0
-rw-rw-rw- 1 wolf wolf 8 May 23 11:33 hello
wolf@BALTHAZAR:~/work/commit-push$ cat ex00/hello
world
wolf@BALTHAZAR:~/work/commit-push$
```

After this, you will need to use git. First thing first, you may need to configure your name and your email with git, this way:

```
$ git config --global user.name "Your Name"
$ git config --global user.email "your@email.co.kr"
```

Then you can start check files you want to commit with `git status`

```
wolf@BALTHAZAR:~/work/commit-push$ git status
On branch master
Your branch is up-to-date with 'origin/master'.
Untracked files:
  (use "git add <file>..." to include in what will be committed)

        ex00/

nothing added to commit but untracked files present (use "git add" to track)
wolf@BALTHAZAR:~/work/commit-push$
```

To add a file you want to commit: `git add file1 file2 directory1`. So here you you will need to do:

```
git add ./ex00/
```

Then you can check again with `git status`, when you are sure you can proceed with `git commit`

```
git commit -m "your commit message"
```

when you are ready to push, always pull.

```
git pull
```

then push

```
git push
```

And congratulation! You completed your first exercise. I will put series of exercises on new pages.